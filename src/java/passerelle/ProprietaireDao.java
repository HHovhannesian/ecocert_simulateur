/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passerelle;

import beans.Proprietaire;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import tests.HibernateUtil;
import tests.HibernateUtil;

/**
 *
 * @author hugo.hovhannessian
 */
public class ProprietaireDao {

    //Récupère un propriétaire suivant son id
    public static Proprietaire getProprietaire(int id) {
        Session session = HibernateUtil.getSession();
        Proprietaire unProprio;
        Query query = session.createQuery("from Proprietaire where id =" + id);
        unProprio = (Proprietaire) query.uniqueResult();
        session.close();
        return unProprio;
    }

    //Récupère un propriétaire suivant son id et son mot de passe
    public static Proprietaire getProprietaireConnexion(String numRegistre, String motdepasse) {
        Session session = HibernateUtil.getSession();
        Proprietaire unProprio;
        Query query = session.createQuery("from Proprietaire where numRegistre ='" + numRegistre
                + "' and mdp ='" + DigestUtils.sha1Hex(motdepasse) + "'");
        unProprio = (Proprietaire) query.uniqueResult();
        session.close();
        return unProprio;
    }

    //Vérifie si un propriétaire possédant le numéro de registre existe
    //Renvoie true s'il existe, false s'il n'existe pas
    public static boolean proprietaireExists(String numRegistre) {
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("from Proprietaire where numRegistre ='" + numRegistre + "'");
        Proprietaire unProprio = (Proprietaire) query.uniqueResult();
        session.close();
        if (unProprio == null) {
            return false;
        }
        return true;
    }

    //Ajoute un propriétaire dans la table à partir de son objet Proprietaire
    public static void setProprietaire(Proprietaire unProprio) {
        Session session = HibernateUtil.getSession();
        try {
            //Ajout du propriétaire dans une transaction
            Transaction trans = session.beginTransaction();
            //Transformation du mot de passe en SHA1
            unProprio.setMdp(DigestUtils.sha1Hex(unProprio.getMdp()));
            session.merge(unProprio);
            trans.commit();

            session.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
