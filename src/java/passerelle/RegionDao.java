
package passerelle;

import beans.Region;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import tests.HibernateUtil;
import tests.HibernateUtil;

public class RegionDao {
    
    //Récupere une region suivant son id.
    public static Region getRegion(int id){
        Session session = HibernateUtil.getSession();
        Region laRegion;
        Query query = session.createQuery("from Region where numRegion ="+id);
        laRegion = (Region)query.uniqueResult();
        return laRegion;
    }
    
    //Récupere les regions.
    public static ArrayList<Region> getLesRegions(){
        Session session = HibernateUtil.getSession();
        ArrayList<Region> lesRegions;
        
        Query query = session.createQuery("from Region");
        lesRegions = (ArrayList<Region>)query.list();
        return lesRegions;
    }
}
