/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passerelle;

import beans.Proprietaire;
import beans.Simulation;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import tests.HibernateUtil;
import tests.HibernateUtil;

/**
 *
 * @author ines.feugier
 */
public class SimulationDao {

    //Récupère toutes les simulations.
    public static List<Simulation> getSimulations() {
        Session session = HibernateUtil.getSession();
        return session.createQuery("from Simulation")
                .list();
    }

    //Récupère une simulation suivant son id.
    public static Simulation getSimulation(int id) {
        Session session = HibernateUtil.getSession();
        Simulation laSimulation;
        Query query = session.createQuery("from Simulation where id =" + id);
        laSimulation = (Simulation) query.uniqueResult();
        return laSimulation;
    }

    /**
     * Récupère les simulations d'un propriétaire
     *
     * @param proprio Le propriétaire concerné
     * @return List<Simulation> La liste des simulations du propriétaire
     */
    public static List<Simulation> getSimulationsProprio(Proprietaire proprio) {
        Session session = HibernateUtil.getSession();
        return session.createQuery("from Simulation where numProprietaire =" + proprio.getId())
                .list();
    }

    //Ajoute/Modifie une simulation
    public static void setSimulation(Simulation simul) {
        Session session = HibernateUtil.getSession();
        try {
            Transaction trans = session.beginTransaction();

            session.merge(simul);

            trans.commit();

            session.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Supprime une simulation
    public static void deleteSimulation(int id) {
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("from Simulation where id =" + id);
        Simulation simul = (Simulation) query.uniqueResult();
        try {
            Transaction trans = session.beginTransaction();
            session.delete(simul);
            trans.commit();
            session.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
