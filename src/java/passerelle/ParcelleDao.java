
package passerelle;

import beans.Parcelle;
import beans.Simulation;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import tests.HibernateUtil;
import tests.HibernateUtil;

public class ParcelleDao {
    
    //Récupere une region suivant son id.
    public static Parcelle getParcelle(int id){
        Session session = HibernateUtil.getSession();
        Parcelle laParcelle;
        Query query = session.createQuery("from Parcelle where id ="+id);
        laParcelle = (Parcelle)query.uniqueResult();
        return laParcelle;
    }
    
    //Récupère la liste des parcelles d'une simulation.
    public static List<Parcelle> getParcellesSimulation(Simulation laSim){
        Session session = HibernateUtil.getSession();
        ArrayList<Parcelle> lesParcelles;
        Query query = session.createQuery("from Parcelle where idSimulation ="+laSim.getId());
        lesParcelles = (ArrayList<Parcelle>)query.list();
        return lesParcelles;
    }
    
    //Récupere les regions.
    public static ArrayList<Parcelle> getLesParcelles(){
        Session session = HibernateUtil.getSession();
        ArrayList<Parcelle> lesParcelles;
        
        Query query = session.createQuery("from Parcelle");
        lesParcelles = (ArrayList<Parcelle>)query.list();
        return lesParcelles;
    }
}
