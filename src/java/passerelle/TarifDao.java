package passerelle;

import beans.Tarif;
import org.hibernate.Query;
import org.hibernate.Session;
import tests.HibernateUtil;
import tests.HibernateUtil;

/**
 *
 * @author ines.feugier
 */
public class TarifDao {
    //Récupère un tarif en fonction de l'identifiant de la culture et de son état
    public static Tarif getTarif(int numCulture, int numEtat){
        Session session = HibernateUtil.getSession();
        Tarif leTarif;
        Query query = session.createQuery("from Tarif where numCulture ="+numCulture
                + " and numEtat =" +numEtat);
        leTarif = (Tarif)query.uniqueResult();
        return leTarif;
    }
}
