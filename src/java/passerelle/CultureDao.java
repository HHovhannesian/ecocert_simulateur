
package passerelle;

import beans.Culture;
import beans.CultureAnnuelle;
import beans.CulturePerenne;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import tests.HibernateUtil;
import tests.HibernateUtil;

public class CultureDao {
    //Récupere une culture suivant sont id.
    public static CulturePerenne getCulture(int id){
        Session session = HibernateUtil.getSession();
        CulturePerenne laCulture;
        Query query = session.createQuery("from Culture where numCulture ="+id);
        laCulture = (CulturePerenne)query.uniqueResult();
        return laCulture;
    }
    
    //Récupere les cultures.
    public static ArrayList<CulturePerenne> getLesCultures(){
        Session session = HibernateUtil.getSession();
        ArrayList<CulturePerenne> lesCulture;
        Query query = session.createQuery("from Culture where TypeCulture = 1");
        lesCulture = (ArrayList<CulturePerenne>)query.list();
        return lesCulture;
    }
   
}
