/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "proprietaire")
public class Proprietaire implements Serializable {

//<editor-fold defaultstate="collapsed" desc="variables">
    //Notifie un identifiant auto-incrementé.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "numRegistre", nullable = false)
    private String numRegistre;

    @Column(name = "nomProprietaire", nullable = false)
    private String nomProprietaire;

    @Column(name = "dateDebutActivite", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dateDebutActivite;

    @Column(name = "mdp", nullable = false)
    private String mdp;

    //Notifie une liaison (Clé etrangere) vers la table cible (Simulation)
    @OneToMany(mappedBy = "leProp")
    private List<Simulation> lesSimulations;
//</editor-fold>


//<editor-fold defaultstate="collapsed" desc="constructeurs">
    public Proprietaire() {
    }

    public Proprietaire(String numRegistre, String nomProprietaire, Calendar dateDebutActivite) {
        this.numRegistre = numRegistre;
        this.nomProprietaire = nomProprietaire;
        this.dateDebutActivite = dateDebutActivite;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public int getId() {
        return id;
    }

    public String getNumRegistre() {
        return numRegistre;
    }

    public void setNumRegistre(String numRegistre) {
        this.numRegistre = numRegistre;
    }

    public String getNomProprietaire() {
        return nomProprietaire;
    }

    public void setNomProprietaire(String nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
    }

    public Calendar getDateDebutActivite() {
        return dateDebutActivite;
    }

    public void setDateDebutActivite(Calendar dateDebutActivite) {
        this.dateDebutActivite = dateDebutActivite;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public List<Simulation> getLesSimulations() {
        return lesSimulations;
    }

    public void setLesSimulations(List<Simulation> lesSimulations) {
        this.lesSimulations = lesSimulations;
    }

//</editor-fold>
    
    public void ajoutSimulations(Simulation simul){
        lesSimulations.add(simul);
        simul.setLeProp(this);
    }
}
