package beans;

import cles_composites.PK_MoisRecolte;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="MoisRecolte")
public class MoisRecolte implements Serializable{
    //Notifie que l'identifiant est exportable (il est composé des propriétés de l'objet PK_MoisRecolte).
    @EmbeddedId
    private PK_MoisRecolte idComposite;
    
    //Notifie une liaison (Clé etrangere) vers la table cible (Region)
    @ManyToOne
    @JoinColumn(name="numRegion", nullable=false, insertable=false, updatable=false)
    private Region saRegion;
    
    @Column(name="numMois", nullable=false)
    private int numMois;
    
//<editor-fold defaultstate="collapsed" desc="constructeur">
    public MoisRecolte(){
    }
    public MoisRecolte(Region uneRegion, int unNumMois){
        saRegion = uneRegion;
        numMois = unNumMois;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public Region getSaRegion() {
        return saRegion;
    }
    public int getNumMois() {
        return numMois;
    }
    public void setSaRegion(Region saRegion) {
        this.saRegion = saRegion;
    }
    
    public void setNumMois(int numMois) {
        this.numMois = numMois;
    }
//</editor-fold>
}
