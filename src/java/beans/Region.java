package beans;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="Region")
public class Region implements Serializable{
    //Notifie un identifiant auto-incrementé.
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int numRegion ;
    
    @Column(name="nomRegion", nullable=false)
    private String nomRegion;
    
//<editor-fold defaultstate="collapsed" desc="constructeurs">
    public Region(){
    }
    
    public Region(int numRegion, String nomRegion) {
        this.numRegion = numRegion;
        this.nomRegion = nomRegion;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public int getNumRegion() {
        return numRegion;
    }
    
    public String getNomRegion() {
        return nomRegion;
    }
    
    public void setNumRegion(int numRegion) {
        this.numRegion = numRegion;
    }
    
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }
//</editor-fold>  
}
