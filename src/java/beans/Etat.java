package beans;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "Etat")
public class Etat implements Serializable {

    //Notifie un identifiant auto-incrementé.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numEtat;

    @Column(name = "libelle", nullable = false)
    private String libelle;
    //<editor-fold defaultstate="collapsed" desc="constructeurs">

    public Etat() {
    }

    public Etat(int numEtat, String libelle) {
        this.numEtat = numEtat;
        this.libelle = libelle;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter-setter">
    public String getLibelle() {
        return libelle;
    }

    public int getNumEtat() {
        return numEtat;
    }

    public void setNumEtat(int numEtat) {
        this.numEtat = numEtat;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    //</editor-fold>

}
