package beans;

import classes_techniques.LigneCA;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import javax.persistence.*;

@Entity
@Table(name = "simulation")
public class Simulation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "dateDebConv", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dateDebConv;

    @Column(name = "dateSimulation", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dateSimulation;

    //Notifie une liaison (Clé etrangere) vers la table cible (Simulation)
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "numProprietaire", nullable = false)
    private Proprietaire leProp;

    @ManyToOne
    @JoinColumn(name = "numRegion", nullable = false)
    private Region laRegion;

    //Notifie une liaison (Clé etrangere) vers la table cible 
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "idSimulation")
    private List<Parcelle> lesParcelles;

    //<editor-fold defaultstate="collapsed" desc="constructeurs">
    public Simulation() {
        lesParcelles = new ArrayList<>();
    }

    public Simulation(Proprietaire leProp, Calendar dateDebConv, Calendar dateSimulation, Region laRegion) {
        this.leProp = leProp;
        this.dateDebConv = dateDebConv;
        this.dateSimulation = dateSimulation;
        this.laRegion = laRegion;
        lesParcelles = new ArrayList<>();
    }

    public Simulation(Calendar debConv, Calendar dateSimul, Region uneRegion) {
        dateSimulation = dateSimul;
        dateDebConv = debConv;
        laRegion = uneRegion;
        lesParcelles = new ArrayList<>();
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getter-setter">
    public int getId() {
        return id;
    }

    public Proprietaire getLeProp() {
        return leProp;
    }

    public void setLeProp(Proprietaire leProp) {
        this.leProp = leProp;
    }

    public Calendar getDateDebConv() {
        return dateDebConv;
    }

    public void setDateDebConv(Calendar dateDebConv) {
        this.dateDebConv = dateDebConv;
    }

    public Calendar getDateSimulation() {
        return dateSimulation;
    }

    public void setDateSimulation(Calendar dateSimulation) {
        this.dateSimulation = dateSimulation;
    }

    public Region getLaRegion() {
        return laRegion;
    }

    public void setLaRegion(Region laRegion) {
        this.laRegion = laRegion;
    }

    public List<Parcelle> getLesParcelles() {
        return lesParcelles;
    }

    public void setLesParcelles(List<Parcelle> lesParcelles) {
        this.lesParcelles = lesParcelles;
    }
//</editor-fold>

    //Retourne les chiffre d'affaires de la simulation
    public ArrayList<LigneCA> getLesCA() {

        ArrayList<LigneCA> lesLignesCA = new ArrayList<LigneCA>();
        for (Parcelle uneParcelle : lesParcelles) {
            for (LigneCA uneLigne : uneParcelle.getLignesCA(laRegion, dateDebConv)) {
                lesLignesCA.add(uneLigne);
            }
        }
        return lesLignesCA;
    }
}
