package beans;

import classes_techniques.LigneCA;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//Notifie que cet objet sera notifié par la valeur 1 dans la table mère.
@DiscriminatorValue(value = "1")
public class CulturePerenne extends Culture implements Serializable {

//<editor-fold defaultstate="collapsed" desc="constructeur">
    public CulturePerenne() {
        super();
    }
    
    public CulturePerenne(int numCulture, String libelle, float rentabilite) {
        super(numCulture, libelle, rentabilite);
    }
//</editor-fold>

    //Fonction qui permet de récupérer une ligne de chiffre d'affaires de la recolte suivant sa surface,
    //sa region, son numero et sa date de debut de conversion.
    public LigneCA getCA(float surface, Region uneRegion, int numRec, Calendar dateDebutConv) {
        Calendar dateRec = getDateRec(uneRegion, numRec, dateDebutConv);
        int numEtat = 0;
        int nbMois = dateRec.get(Calendar.MONTH) - dateDebutConv.get(Calendar.MONTH);

        if (nbMois < 0) {// si le mois de récolte est inferieur au mois de debut de conversion (donc resultat négatif)
            nbMois *= -1;//alors on le rend positif.
            nbMois = 12 - nbMois;//et on recalcule la difference.
        }
        nbMois = nbMois + 12 * (numRec - 1);// On récupère le nombre de mois total
        if (nbMois <= 12) {
            numEtat = 1;
        } else if (nbMois > 12 && nbMois <= 36) {
            numEtat = 2;
        } else {
            numEtat = 3;
        }
        LigneCA uneLigneCA = new LigneCA(dateRec, getCARec(numEtat, surface), super.getUnTarif(numEtat).getSonEtat());
        return uneLigneCA;
    }
}
