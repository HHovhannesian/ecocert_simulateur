package beans;

import classes_techniques.LigneCA;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
//Notifie que cet objet sera notifié par la valeur 2 dans la table mere.
@DiscriminatorValue(value = "2")
public class CultureAnnuelle extends Culture implements Serializable {

    //Notifie une liaison (Clé etrangere) de la table cible
    @OneToMany
    @JoinColumn(name = "numCulture")
    private List<MoisSemis> lesMoisSemis;

    //<editor-fold defaultstate="collapsed" desc="Constructeurs">  
    public CultureAnnuelle() {
        super();
    }

    public CultureAnnuelle(int numCulture, String libelle, float rentabilite) {
        super(numCulture, libelle, rentabilite);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter-setter"> 
    public List<MoisSemis> getLesMoisSemis() {
        return lesMoisSemis;
    }

    public void setLesMoisSemis(List<MoisSemis> lesMoisSemis) {
        this.lesMoisSemis = lesMoisSemis;
    }

    //</editor-fold>
    //Fonction qui permet de récupérer une ligne de chiffre d'affaires de la récolte suivant sa surface,
    //sa region, son numero et sa date de debut de conversion.
    public LigneCA getCA(float surface, Region uneRegion, int numRec, Calendar dateDebutConv) {
        Calendar dateRec = super.getDateRec(uneRegion, numRec, dateDebutConv);
        int numEtat = 0;
        int numMoisSemis = 0;
        int nbMois = dateRec.get(Calendar.MONTH) - dateDebutConv.get(Calendar.MONTH);//Recupere la difference
        if (nbMois < 0) {// si le mois de recolte est inferieur au mois de debut de conversion (donc resultat négatif)
            nbMois *= -1;//alors on le rend positif.
            nbMois = 12 - nbMois;//et on recalcule la différence.
        }
        nbMois = nbMois + 12 * (numRec - 1);// On récupère le nombre de mois total
        for (MoisSemis unMois : lesMoisSemis) {
            if (unMois.getSaRegion().getNumRegion() == uneRegion.getNumRegion()) {
                numMoisSemis = unMois.getNumMois();
            }
        }
        if (nbMois <= 12) {
            numEtat = 1;
        } else if (nbMois > 12 && nbMois <= 24) {
            numEtat = 2;
        } else {
            if (numMoisSemis < dateDebutConv.get(Calendar.MONTH) && numRec == 3) {
                numEtat = 2;
            } else {
                numEtat = 3;
            }
        }
        LigneCA uneLigneCA = new LigneCA(dateRec, getCARec(numEtat, surface), super.getUnTarif(numEtat).getSonEtat());
        return uneLigneCA;
    }
}
