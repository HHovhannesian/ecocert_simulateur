
package beans;

import classes_techniques.LigneCA;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.*;


@Entity
@Table(name = "parcelle")
public class Parcelle implements Serializable{
   
    //Notifie un identifiant auto-incrementé.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "surface", nullable = false)
    private float surface;
    
    @Column(name = "typeParcelle", nullable = false)
    private int typeParcelle;

    @ManyToMany
    @JoinTable(name="parcelle_ca",
            joinColumns={@JoinColumn(name="idParcelle")},
            inverseJoinColumns={@JoinColumn(name="idCulture")})
    private List<CultureAnnuelle> lesCulturesA= new ArrayList<CultureAnnuelle>();

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="numCultureP")
    private CulturePerenne laCultureP;


//<editor-fold defaultstate="collapsed" desc="constructeurs">
    public Parcelle() {
    }
    
    public Parcelle(float uneSurface, int type){
        surface = uneSurface;
        typeParcelle = type;
        laCultureP = new CulturePerenne();
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public int getId() {
        return id;
    }
    
    public float getSurface() {
        return surface;
    }
    
    public void setSurface(float surface) {
        this.surface = surface;
    }
    
    public int getTypeParcelle() {
        return typeParcelle;
    }
    
    public void setTypeParcelle(int typeParcelle) {
        this.typeParcelle = typeParcelle;
    }
    public CulturePerenne getLaCultureP(){
        return laCultureP;
    }
    public void setLaCultureP(CulturePerenne laCulture){
        this.laCultureP = laCulture;
    }
    public List<CultureAnnuelle> getLesCultures() {
        return lesCulturesA;
    }
    
    public void setLesCultures(List<CultureAnnuelle> lesCultures) {
        this.lesCulturesA = lesCultures;
    }
//</editor-fold>
    
    //Retourne les lignes de chiffre d'affaires suivant la region et la date de début de conversion.
    public ArrayList<LigneCA> getLignesCA(Region laRegion, Calendar dateDebConv){
        ArrayList<LigneCA> lesLignesCA = new ArrayList<LigneCA>();
        if(typeParcelle == 1){
            //traitement d'une parcelle avec une culture perenne
            for(int i=1; i<5; i++){
               lesLignesCA.add(laCultureP.getCA(surface, laRegion, i, dateDebConv));
            }
        }else{
            //traitement d'une parcelle avec des cultures annuelles
            int i = 1;
            for(CultureAnnuelle laCultureA: lesCulturesA){
                lesLignesCA.add(laCultureA.getCA(surface, laRegion, i, dateDebConv));
                i++;
            }
        }
        return lesLignesCA;
       
        
    }


    public void ajouterCultureA (CultureAnnuelle cultureA){
        lesCulturesA.add(cultureA);
    }
}
