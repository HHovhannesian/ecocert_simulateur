package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.*;
import static javax.persistence.DiscriminatorType.INTEGER;
import static javax.persistence.InheritanceType.SINGLE_TABLE;
import passerelle.TarifDao;

//Classe qui permet de gérer une culture.
//La persistance de données se fait avec MariaDB via le framework hibernate.
@Entity
@Table(name = "Culture")
//Notifie que la classe et ses classes filles seront une unique table.
@Inheritance(strategy = SINGLE_TABLE)
//Notifie que dans la table se trouvera une nouvelle propriété avec un type particulier.
@DiscriminatorColumn(name = "TypeCulture", discriminatorType = INTEGER)
public class Culture implements Serializable {

    //Notifie un identifiant auto-incrementé.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int numCulture;

    @Column(name = "libelle", nullable = false)
    String libelle;

    @Column(name = "rentabilite")
    float rentabilite;

    //Notifie une liaison (Clé etrangère) de la table cible
    @OneToMany
    @JoinColumn(name = "numCulture")
    private List<MoisRecolte> lesMoisRecoltes = new ArrayList<>();

    //Notifie une liaison (Clé etrangère) de la table cible
    @OneToMany
    @JoinColumn(name = "numCulture")
    private List<Tarif> lesTarifs = new ArrayList<>();

    //<editor-fold defaultstate="collapsed" desc="Constructeurs">   
    public Culture() {
    }

    public Culture(int numCulture, String libelle, float rentabilite) {
        this.numCulture = numCulture;
        this.libelle = libelle;
        this.rentabilite = rentabilite;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getter-setter">  

    public int getNumCulture() {
        return numCulture;
    }

    public void setNumCulture(int numCulture) {
        this.numCulture = numCulture;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getRentabilite() {
        return rentabilite;
    }

    public void setRentabilite(float rentabilite) {
        this.rentabilite = rentabilite;
    }

    public List<MoisRecolte> getLesMoisRecoltes() {
        return lesMoisRecoltes;
    }

    public void setLesMoisRecoltes(List<MoisRecolte> lesMoisRecoltes) {
        this.lesMoisRecoltes = lesMoisRecoltes;
    }

    public List<Tarif> getLesTarifs() {
        return lesTarifs;
    }

    public void setLesTarifs(List<Tarif> lesTarifs) {
        this.lesTarifs = lesTarifs;
    }
    //</editor-fold>
        

    //fonction qui permet de récupérer un tarif selon l'état de la culture

    public Tarif getUnTarif(int numEtat) {
        for (Tarif unTarif : lesTarifs) {
            if (unTarif.getSonEtat().getNumEtat() == numEtat) {
                return unTarif;
            }
        }
        return null;
    }

    //Fonction qui permet de récupérer la date de recolte suivant la region, 
    //le numéro de récolte et la date de début de conversion.
    public Calendar getDateRec(Region laRegion, int numRec, Calendar dateDebConv) {
        Calendar date = Calendar.getInstance();
        for (MoisRecolte unMois : lesMoisRecoltes) {
            if (unMois.getSaRegion().getNumRegion() == laRegion.getNumRegion()) {
                int annee = dateDebConv.get(Calendar.YEAR);
                if (dateDebConv.get(Calendar.MONTH) >= unMois.getNumMois()) {
                    annee += 1;
                }
                if (numRec > 0) {
                    date.set(annee + numRec - 1, unMois.getNumMois() - 1, 1);
                }
            }
        }
        return date;
    }

    //Fonction qui permet de récupérer le chiffre d'affaires de la récolte
    //suivant son numéro d'état et sa surface.
    public float getCARec(int numEtat, float surface) {
        //Récupération du tarif (prix/unité) pour une culture d'un certain état
        float prixVente = TarifDao.getTarif(this.numCulture, numEtat).getPrixVente();
        //Retourne le CA (= surface (hectares) x rentabilité (unités/hectare) x tarif (prix/unité))
        return surface * this.rentabilite * prixVente;
    }
}