package beans;

import cles_composites.PK_Tarif;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Tarif")
public class Tarif implements Serializable{
    //Notifie que l'identifiant est exportable (il est composé des propriétés de l'objet PK_Tarif).
    @EmbeddedId
    private PK_Tarif idComposite;
    
    //Notifie une liaison (Clé etrangere) vers la table cible (Etat)
    @ManyToOne 
    @JoinColumn(name="numEtat", nullable=false, insertable=false, updatable=false)
    private Etat sonEtat;
    
    @Column(name="prixVente", nullable=false)
    private float prixVente;
    
//<editor-fold defaultstate="collapsed" desc="constructeurs">
    public Tarif(){
    }
    public Tarif(Etat unEtat, float unPrixVente){
        sonEtat = unEtat;
        prixVente = unPrixVente;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public Etat getSonEtat() {
        return sonEtat;
    }
    public float getPrixVente() {
        return prixVente;
    }
    
    public void setSonEtat(Etat sonEtat) {
        this.sonEtat = sonEtat;
    }
    
    public void setPrixVente(float prixVente) {
        this.prixVente = prixVente;
    }
//</editor-fold>
    
}
