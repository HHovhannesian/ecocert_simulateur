package classes_techniques;

import beans.Etat;
import java.util.Calendar;

public class LigneCA {

    private Calendar laDateRec;
    private Float leCA;
    private Etat letat;

//<editor-fold defaultstate="collapsed" desc="constructeurs">
    public LigneCA(Calendar dateRec, Float chiffreA) {
        laDateRec = dateRec;
        leCA = chiffreA;
    }
    
    public LigneCA(Calendar dateRec, Float chiffreA, Etat etat) {
        laDateRec = dateRec;
        leCA = chiffreA;
        this.letat = etat;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getter-setter">
    public Etat getLetat() {
        return letat;
    }
    
    public void setLetat(Etat letat) {
        this.letat = letat;
    }
    
    public Calendar getLaDateRec() {
        return laDateRec;
    }
    
    public void setLaDateRec(Calendar uneDate) {
        laDateRec = uneDate;
    }
    
    public Float getCA() {
        return leCA;
    }
    
    public void setLeCA(Float unCA) {
        leCA = unCA;
    }
//</editor-fold>
    
    public int getAnRec() {
        return laDateRec.get(Calendar.YEAR);
    }

}
