/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.simulation;

import beans.*;
import classes_techniques.LigneCA;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.CultureDao;
import passerelle.RegionDao;

/**
 *
 * @author hugo.hovhannessian
 */
public class SimulationPerenneServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Instanciation des variables
        ArrayList<Region> lesRegions = RegionDao.getLesRegions();
        ArrayList<CulturePerenne> lesCulturesP = CultureDao.getLesCultures();

        //Ajout des variables a la requêt permettant de les transmettre a la vue
        req.setAttribute("lesRegions", lesRegions);
        req.setAttribute("lesCultures", lesCulturesP);

        //Affichage de la vue
        this.getServletContext().getRequestDispatcher("/WEB-INF/Enregistrement/saisie.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //<editor-fold defaultstate="collapsed" desc="Instanciation des variables récupérer">
            Region numRegion = RegionDao.getRegion(Integer.parseInt(req.getParameter("region")));
            String dateDebConv = req.getParameter("dateDebConv");

            int sufaceP1 = Integer.parseInt(req.getParameter("sufaceP1"));
            CulturePerenne cultureP1 = CultureDao.getCulture(Integer.parseInt(req.getParameter("cultureP1")));

            int sufaceP2 = Integer.parseInt(req.getParameter("sufaceP2"));
            CulturePerenne cultureP2 = CultureDao.getCulture(Integer.parseInt(req.getParameter("cultureP2")));
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Création et enregistrement des parcelles">
            ArrayList<Parcelle> lesParcelles = new ArrayList<Parcelle>();
            Parcelle p1 = new Parcelle();
            p1.setSurface(sufaceP1);
            p1.setLaCultureP(cultureP1);

            Parcelle p2 = new Parcelle();
            p2.setSurface(sufaceP2);
            p2.setLaCultureP(cultureP2);
            
            lesParcelles.add(p1);
            lesParcelles.add(p2);
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Convertion de la date en Calendar">
            Calendar dateDebutConversion = Calendar.getInstance();
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
                dateDebutConversion.setTime(sdf.parse(dateDebConv));
            }catch(Exception e){
                System.out.println(e.toString());
            }
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Récupération de la date du jours">
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String strToday = format.format(date);
            
            Calendar today = Calendar.getInstance();
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
                today.setTime(sdf.parse(strToday));
            }catch(Exception e){
                System.out.println(e.toString());
            }
            
        //</editor-fold>
        
        Simulation laSimul = new Simulation(dateDebutConversion, today, numRegion);
        laSimul.setLesParcelles(lesParcelles);
        
        /*HashMap<Parcelle, ArrayList<LigneCA>> listeParcelle= new HashMap<Parcelle, ArrayList<LigneCA>>();
        for(Parcelle uneParcelle : lesParcelles){
           //<editor-fold defaultstate="collapsed" desc="Enregistrement des chiffres d'affaires">
            ArrayList<LigneCA> lesCAParcelle = new ArrayList<LigneCA>();
            for(int i = 1; i < 5; i++ ){
                lesCAParcelle.add(uneParcelle.getLaCultureP().getCA(uneParcelle.getSurface(), numRegion, i, dateDebutConversion));
            }
            //</editor-fold> 
            listeParcelle.put(uneParcelle, lesCAParcelle);
        }*/
        
        
        //<editor-fold defaultstate="collapsed" desc="Test">
        //</editor-fold>
        
        //System.out.println(listeParcelle);
        //<editor-fold defaultstate="collapsed" desc="Création des attributs">
            //req.setAttribute("listeParcelle", listeParcelle);
            req.setAttribute("simulation", laSimul);
        //</editor-fold>

        this.getServletContext().getRequestDispatcher("/WEB-INF/Enregistrement/recapitulatifSaisie.jsp")
                .forward(req, resp);

    }

}
