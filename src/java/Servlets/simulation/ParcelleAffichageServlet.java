/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.simulation;

import beans.*;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.CultureDao;
import passerelle.RegionDao;

/**
 *
 * @author hugo.hovhannessian
 */
public class ParcelleAffichageServlet extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Instanciation des variables
        ArrayList<Region> lesRegions = RegionDao.getLesRegions();
        ArrayList<CulturePerenne> lesCultures = CultureDao.getLesCultures();

        //Ajout des variables a la requêt permettant de les transmettre a la vue
        req.setAttribute("lesRegions", lesRegions);
        req.setAttribute("lesCultures", lesCultures);

        //Affichage de la vue
        this.getServletContext().getRequestDispatcher("/WEB-INF/Enregistrement/simulations.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Vérification des saisies
        Region numRegion = RegionDao.getRegion(Integer.parseInt(req.getParameter("region")));
        String dateDebConv = req.getParameter("dateDebConv");
        int sufaceP1 = Integer.parseInt(req.getParameter("sufaceP1"));
        Culture cultureP1 = CultureDao.getCulture(Integer.parseInt(req.getParameter("cultureP1")));
        int sufaceP2 = Integer.parseInt(req.getParameter("sufaceP2"));
        Culture cultureP2 = CultureDao.getCulture(Integer.parseInt(req.getParameter("cultureP2")));

        //Création des attributs
        req.setAttribute("numRegion", numRegion.getNomRegion());
        req.setAttribute("dateDebConv", dateDebConv);
        req.setAttribute("sufaceP1", sufaceP1);
        req.setAttribute("cultureP1", cultureP1.getLibelle());
        req.setAttribute("sufaceP2", sufaceP2);
        req.setAttribute("cultureP2", cultureP2.getLibelle());

        this.getServletContext().getRequestDispatcher("/WEB-INF/Enregistrement/recapitulatifSaisie.jsp")
                .forward(req, resp);

    }
}
