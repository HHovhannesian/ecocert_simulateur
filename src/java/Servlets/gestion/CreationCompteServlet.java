/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.gestion;

import beans.Proprietaire;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.ProprietaireDao;

/**
 *
 * @author ines.feugier
 */
@WebServlet(name = "CreationCompteServlet", urlPatterns = {"/createAccount"})
public class CreationCompteServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/enregistrementProprio.jsp")
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Vérification des saisies
        boolean saisieCorrecte = true;
        String numRegistre = request.getParameter("numRegistre");
        String nom = request.getParameter("nom");
        String sDateDebut = request.getParameter("dateDebut");
        String motdepasse = request.getParameter("motdepasse");
        String motdepasseConf = request.getParameter("motdepasseConf");

        //Si les attributs n'ont pas été renseignés
        if (numRegistre.equals("") || nom.equals("") || sDateDebut.equals("")) {
            saisieCorrecte = false;
            request.setAttribute("proprioIncorrect", "Veuillez saisir toutes les données");
        }
        //Si la confirmation du mot de passe est incorrecte
        if (!motdepasse.equals(motdepasseConf)) {
            saisieCorrecte = false;
            request.setAttribute("proprioIncorrect", "Le mot de passe est incorrect");
        }
        //Si le numéro de registre existe déjà dans la base de données
        if(ProprietaireDao.proprietaireExists(numRegistre)){
            saisieCorrecte = false;
            request.setAttribute("proprioIncorrect", "Le numéro de registre choisi est déjà utilisé");
        }

        //Si les informations saisies sont correctes
        if (saisieCorrecte) {
            //Création du Proprietaire
            Proprietaire unProprio = new Proprietaire();

            Calendar dateDebut = Calendar.getInstance();
            SimpleDateFormat calendarFormat = new SimpleDateFormat("d/M/y", Locale.FRANCE);
            try {
                dateDebut.setTime(calendarFormat.parse(sDateDebut));
            } catch (Exception e) {
                System.out.print(e);
                System.out.print(sDateDebut);
            }

            unProprio.setNumRegistre(numRegistre);
            unProprio.setNomProprietaire(nom);
            unProprio.setDateDebutActivite(dateDebut);
            unProprio.setMdp(motdepasse);
            
            //Ajout du propriétaire à la base de données
            ProprietaireDao.setProprietaire(unProprio);

            //Ajout du propriétaire pour affichage de la page de confirmation
            DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
            request.setAttribute("proprioCree", unProprio);
            request.setAttribute("dateDebutActivite", format1.format(unProprio.getDateDebutActivite().getTime()));

            //Affichage de la vue résumant le dossier créé
            this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/confirmationEnregistrementProprio.jsp")
                    .forward(request, response);
        }
        //Les paramètres sont sauvegardés
        request.setAttribute("numRegistre", numRegistre);
        request.setAttribute("nom", nom);
        request.setAttribute("dateDebut", sDateDebut);
        
        //Affichage de la vue précédente si les informations ne sont pas correctes
        this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/enregistrementProprio.jsp")
                .forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Affiche le formulaire d'enregistrement d'un propriétaire et "
                + "permet son enregistrement dans la base de données";
    }

}
