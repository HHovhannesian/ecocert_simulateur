/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.gestion;

import beans.Parcelle;
import beans.Proprietaire;
import beans.Simulation;
import classes_techniques.LigneCA;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import passerelle.SimulationDao;

/**
 *
 * @author ines.feugier
 */
@WebServlet(name = "AffichageDetailServlet", urlPatterns = {"/detailSimulation"})
public class AffichageDetailServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Proprietaire leProprio = (Proprietaire) session.getAttribute("leProprietaire");
        //Dans le cas où l'utilisateur est connecté
        if (leProprio != null) {
            //On récupère l'identifiant de la simulation puis la simulation
            String strIdSim = request.getParameter("idSim");
            int idSim = Integer.parseInt(strIdSim.replaceAll("\\s+", ""));
            Simulation laSim = SimulationDao.getSimulation(idSim);

            //On récupère les données des simulations dans un ArrayList
            HashMap<String, Object> donneesSim = new HashMap<>();
            DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
            //Ajout des données de la simulation
            donneesSim.put("id", String.valueOf(laSim.getId()));
            donneesSim.put("dateSim", format2.format(laSim.getDateSimulation().getTime()));
            donneesSim.put("dateDebConv", format2.format(laSim.getDateDebConv().getTime()));
            donneesSim.put("region", String.valueOf(laSim.getLaRegion().getNomRegion()));
            donneesSim.put("nbParcelles", laSim.getLesParcelles().size());

            //Récupération des lignesCA des parcelles dans une liste
            List<ArrayList<LigneCA>> lignesCAParcelles = new ArrayList<>();
            for (Parcelle uneParcelle : laSim.getLesParcelles()) {
                //Ajout de la LigneCA à la liste des lignesCA
                lignesCAParcelles.add(uneParcelle.getLignesCA(laSim.getLaRegion(), laSim.getDateDebConv()));
            }

            //Données passés à la vue JSP
            request.setAttribute("format2", format2);
            request.setAttribute("laSim", donneesSim);
            request.setAttribute("laSimulation", laSim);
            request.setAttribute("lignesCA", lignesCAParcelles);
            request.setAttribute("parcelles", laSim.getLesParcelles());
            
            //Affichage de la vue permettant l'affichage de ces données
            this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/detailSimulation.jsp")
                    .forward(request, response);
        } else {
            //L'utilisateur n'est pas connecté, message d'erreur
            this.getServletContext().getRequestDispatcher("/WEB-INF/pasConnecte.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Permet l'affichage des détails d'une simulation pour un propriétaire connecté";
    }
}
