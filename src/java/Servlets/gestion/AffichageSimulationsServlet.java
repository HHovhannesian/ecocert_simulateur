/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.gestion;

import beans.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import passerelle.SimulationDao;

/**
 *
 * @author ines.feugier
 */
@WebServlet(name = "AffichageSimulationsServlet", urlPatterns = {"/affichageSimulations"})
public class AffichageSimulationsServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Proprietaire leProprio = (Proprietaire) session.getAttribute("leProprietaire");
        //Dans le cas où l'utilisateur est connecté
        if (leProprio != null) {
            //On récupère les données des simulations dans un ArrayList
            List<HashMap<String, String>> donneesSimulations = new ArrayList<>();
            HashMap<String, String> donneesUneSim = new HashMap<>();
            for (Simulation laSim : SimulationDao.getSimulationsProprio(leProprio)) {
                donneesUneSim = new HashMap<>();
                DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                //Ajout des données de la simulation
                donneesUneSim.put("id", String.valueOf(laSim.getId()));
                donneesUneSim.put("dateSim", format1.format(laSim.getDateSimulation().getTime()));
                donneesUneSim.put("dateDebConv", format1.format(laSim.getDateDebConv().getTime()));
                donneesUneSim.put("region", String.valueOf(laSim.getLaRegion().getNomRegion()));
                donneesUneSim.put("nbParcelles", String.valueOf(laSim.getLesParcelles().size()));

                //Ajout de la simulation à la liste des simulations
                donneesSimulations.add(donneesUneSim);
            }
            
            request.setAttribute("lesSimulations", donneesSimulations);
            //Affichage de la vue permettant l'affichage de ces données
            this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/affichageSimulations.jsp")
                    .forward(request, response);
        } else {
            //L'utilisateur n'est pas connecté, message d'erreur
            this.getServletContext().getRequestDispatcher("/WEB-INF/pasConnecte.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Permet l'affichage des simulations pour un propriétaire connecté";
    }

}
