/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.gestion;

import beans.Proprietaire;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import passerelle.ProprietaireDao;

/**
 *
 * @author ines.feugier
 */
@WebServlet(name = "ConnectionProprietaireServlet", urlPatterns = {"/connectionProprio"})
public class ConnectionProprietaireServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/connectionProprio.jsp")
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String numRegistre = request.getParameter("numRegistre");
        String motdepasse = request.getParameter("motdepasse");
        Proprietaire leProprio = ProprietaireDao.getProprietaireConnexion(numRegistre, motdepasse);
        if (leProprio != null) {
            HttpSession session = request.getSession();
            session.setAttribute("leProprietaire", leProprio);
            this.getServletContext().getRequestDispatcher("/index.jsp")
                    .forward(request, response);
        } else {
            request.setAttribute("proprioIncorrect", "Le mot de passe ne correspond pas au numéro de registre, "
                    + "veuillez rééssayer");
            request.setAttribute("numRegistre", numRegistre);
            this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/connectionProprio.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Affiche le formulaire de connection d'un propriétaire et "
                + "l'accès à l'enregistrement d'un propriétaire";
    }// </editor-fold>

}
