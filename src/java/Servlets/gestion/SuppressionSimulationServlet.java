/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.gestion;

import beans.Proprietaire;
import beans.Simulation;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import passerelle.SimulationDao;

/**
 *
 * @author ines.feugier
 */
@WebServlet(name = "SuppressionSimulation", urlPatterns = {"/suppressionSimulation"})
public class SuppressionSimulationServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Proprietaire leProprio = (Proprietaire) session.getAttribute("leProprietaire");
        //Dans le cas où l'utilisateur est connecté
        if (leProprio != null) {
            //On récupère l'identifiant de la simulation puis la simulation
            String strIdSim = request.getParameter("idSim");
            int idSim = Integer.parseInt(strIdSim.replaceAll("\\s+", ""));
            Simulation laSim = (Simulation) SimulationDao.getSimulation(idSim);
            //Si la simulation appartient bien au propriétaire
            if (laSim.getLeProp().getId() == leProprio.getId()) {
                request.setAttribute("laSim", laSim);
                //Affichage de la vue permettant l'affichage de ces données
                this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/confirmationSuppression.jsp")
                        .forward(request, response);
            } else {
                //L'utilisateur n'a pas réalisé cette simulation, message d'erreur
                this.getServletContext().getRequestDispatcher("/WEB-INF/gestion/pasProprietaireSimulation.jsp")
                        .forward(request, response);
            }
        } else {
            //L'utilisateur n'est pas connecté, message d'erreur
            this.getServletContext().getRequestDispatcher("/WEB-INF/pasConnecte.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Permet l'affichage d'un écran demandant la confirmation de la suppression d'une simulation";
    }
}
