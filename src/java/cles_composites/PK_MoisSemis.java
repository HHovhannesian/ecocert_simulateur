package cles_composites;

import java.io.Serializable;
import javax.persistence.Embeddable;
//Classe qui permet du coté de la persistance des données de concaténer les deux propriétés comme identifiant.
@Embeddable
public class PK_MoisSemis implements Serializable{
    private int numCulture;
    private int numRegion;

    public PK_MoisSemis() {
    }
}
