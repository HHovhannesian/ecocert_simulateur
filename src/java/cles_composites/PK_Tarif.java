package cles_composites;

import java.io.Serializable;
import javax.persistence.Embeddable;
//Classe qui permet du coté de la persistance des données de concaténer les deux propriétés comme identifiant.
@Embeddable
public class PK_Tarif implements Serializable{
    private int numCulture;
    private int numEtat;
    
    public PK_Tarif(){
    }
    
}
