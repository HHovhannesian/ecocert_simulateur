/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import passerelle.SimulationDao;
import beans.*;
import java.util.List;
import java.util.Locale;
import passerelle.*;

/**
 *
 * @author ines.feugier
 */
public class TestHibernateSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Lecture de toutes les simulations 
        List<Simulation> lesSim = SimulationDao.getSimulations();

        // <editor-fold defaultstate="collapsed" desc="Test de l'affichage des simulations">
        //Pour chaque simulations ... 
        for (Simulation laSim : lesSim) {

            //On affiche le nom de la zone, du proprio et la date de début de convention
            System.out.println(laSim.getId() 
                    + ": Zone : " + laSim.getLaRegion().getNomRegion()
                    + "\n Proprio : " + laSim.getLeProp().getNomProprietaire()
                    + "\n Date : " + laSim.getDateDebConv().toString()
            );
            int unNumRegion = laSim.getLaRegion().getNumRegion();

            // <editor-fold defaultstate="collapsed" desc="Pour chaque parcelle ... ">
            //Pour chaque parcelle ... 
            for (Parcelle laPar : laSim.getLesParcelles()) {

                //On affiche l'id et la surface de la parcelle
                System.out.println(
                        "  - Parcelle " + laPar.getId() + ": "
                        + laPar.getSurface() + "m² "
                );

                // <editor-fold defaultstate="collapsed" desc="Si la parcelle est une parcelle avec une culture perenne ... ">
                // Si la parcelle est une parcelle avec une culture perenne ... 
                if (laPar.getTypeParcelle() == 1) {
                    //On enregistre la culture en tant que culture perenne
                    CulturePerenne laCultP = laPar.getLaCultureP();
                    System.out.println("    Type culture : " + laCultP.getLibelle());
                    
                    // <editor-fold defaultstate="collapsed" desc="Pour chaques mois de recoltes ... ">
                    //Pour chaques mois de recoltes
                    for (MoisRecolte leMoisR : laCultP.getLesMoisRecoltes()) {
                        //On affiche le mois en fonctions de la region
                        if (leMoisR.getSaRegion().getNumRegion() == unNumRegion) {
                            System.out.println("    - Mois de récolte : "
                                    + leMoisR.getNumMois() + " "
                            );
                        }
                    }
                    // </editor-fold>
                } else {
                    // <editor-fold defaultstate="collapsed" desc="Pour toutes les cultures ... ">
                    //Pour toutes les cultures ...
                    for (Culture laCult : laPar.getLesCultures()) {

                        //On mets la culture en Annuelle
                        CultureAnnuelle laCultA = (CultureAnnuelle) laCult;

                        //On afffiche le type de culture
                        System.out.println("    Type culture : " + laCult.getLibelle());

                        // <editor-fold defaultstate="collapsed" desc="Pour chaque mois de semis ... ">
                        //Pour chaque mois de semis 
                        for (MoisSemis leMoisS : laCultA.getLesMoisSemis()) {
                            //On affiche le mois en fonctions de la region
                            if (leMoisS.getSaRegion().getNumRegion() == unNumRegion) {
                                System.out.println("    - Mois de Semis : "
                                        + leMoisS.getNumMois() + " "
                                );
                            }
                        }
                        // </editor-fold>

                        // <editor-fold defaultstate="collapsed" desc="Pour chaque mois de Recolte ... ">
                        //Pour chaque mois de récolte
                        for (MoisRecolte leMoisR : laCult.getLesMoisRecoltes()) {
                            //On affiche le mois en fonctions de la region
                            if (leMoisR.getSaRegion().getNumRegion() == unNumRegion) {
                                System.out.println("    - Mois de récolte : "
                                        + leMoisR.getNumMois() + " "
                                );
                            }
                        }
                        // </editor-fold>
                    }
                    // </editor-fold>
                }
                // </editor-fold>
                
                System.out.println("");
            }
            // </editor-fold>
        }
        // </editor-fold>
        
    }
}
