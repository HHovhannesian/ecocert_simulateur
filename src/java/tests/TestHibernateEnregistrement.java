/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import passerelle.SimulationDao;
import passerelle.ProprietaireDao;
import passerelle.RegionDao;
import passerelle.CultureDao;
import passerelle.*;
import beans.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.hibernate.*;

/**
 *
 * @author hugo.hovhannessian
 */
public class TestHibernateEnregistrement {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Session s = HibernateUtil.getSession();
        
        //Création d'une simulations
        Simulation uneSimulation = new Simulation();
       
        //Récuperation d'une region, d'un propriétaire, et de deux cultures
        Proprietaire leProp = ProprietaireDao.getProprietaire(1);
        Region laRegion = RegionDao.getRegion(2);
        CulturePerenne culture1 = (CulturePerenne)CultureDao.getCulture(9);
        
        //CultureAnnuelle culture2 = (CultureAnnuelle)CultureDao.getCulture(3);
        //CultureAnnuelle culture3 = (CultureAnnuelle)CultureDao.getCulture(1);
        
        //Création et instanciation de deux parcelles
        List<Parcelle> lesParcelles = new ArrayList<Parcelle>();
        Parcelle parcelle1 = new Parcelle();
        //parcelle1.ajouterCultureA(culture2);
        //parcelle1.ajouterCultureA(culture3);
        parcelle1.setSurface(200);
        parcelle1.setTypeParcelle(0);
        
        Parcelle parcelle2 = new Parcelle();
        parcelle2.setLaCultureP(culture1);
        parcelle2.setSurface(50);
        parcelle2.setTypeParcelle(1);
        
        lesParcelles.add(parcelle2);
        lesParcelles.add(parcelle1);
        
        Calendar dateSimulation = Calendar.getInstance();
        Calendar dateDebConv = Calendar.getInstance();
        
        uneSimulation.setDateDebConv(dateDebConv);
        uneSimulation.setDateSimulation(dateSimulation);
        uneSimulation.setLaRegion(laRegion);            
        uneSimulation.setLeProp(leProp);                
        uneSimulation.setLesParcelles(lesParcelles);    
        
        System.out.println(uneSimulation.getLaRegion().getNomRegion() +" "+  uneSimulation.getLeProp().getNomProprietaire());
       
        SimulationDao.setSimulation(uneSimulation);
       
    }
    
}
