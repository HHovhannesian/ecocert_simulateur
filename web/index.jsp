<%-- 
    Document   : index
    Created on : 25 mars 2021, 16:09:16
    Author     : hugo.hovhannessian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container"><br><br>
            <div class="text-center">
                <h2>Organisme de contrôle & de certification</h2>
                <h4>au service de l'homme et de l'environnement</h4>
                <img class="img-thumbnail mt-4" src="assets/images/accueil.jpg" alt="image accueil" />
            </div>
        </div>
    </body>
</html>