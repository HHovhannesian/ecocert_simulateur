<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- Gestion du menu pour les smartphones -->
    <a class="navbar-brand" href="accueil">
        <img class="img-thumbnail" src="assets/images/ecocert.png" alt="Ecocert" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-ecocert">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Fin gestion du menu pour les smartphones -->

    <!-- Menu d�roulant -->
    <div class="collapse navbar-collapse" id="menu-ecocert">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="accueil">Accueil</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    Simulations
                </a>
                <div class="dropdown-menu">
                    <!-- Bouton permettant d'acc�der � l'affichage des simulations -->
                    <a class="dropdown-item
                       <% if (session.getAttribute("leProprietaire") == null) {%>
                       disabled
                       <%}%>" href="affichageSimulations">Affichage des simulations</a>
                    <a class="dropdown-item" href="simulation">Simulations p�rennes</a>
                </div>
            </li>
        </ul>

        <div class="navbar-nav ml-auto">
            <%
                if (session.getAttribute("leProprietaire") == null) {
            %>
            <li class="nav-item">
                <a href="connectionProprio" class="nav-link">
                    <span class="fas fa-user"> Se connecter</span>
                </a>
            </li>
            <%
            } else {
            %>
            <li class="navbar-text">${leProprietaire.nomProprietaire}</li>
            <li class="nav-item">
                <a href="deconnexionProprio" class="nav-link">
                    <span class="fas fa-user"> Se d�connecter</span >
                </a>
            </li>
            <%
                }
            %>
            <br><br>
        </div>
    </div>
</nav>