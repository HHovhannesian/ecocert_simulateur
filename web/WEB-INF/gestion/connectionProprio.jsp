<%-- 
    Document   : connectionProprio
    Created on : 29 mars 2021, 11:47:56
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container text-center">
            <div class="my-auto">
                <h2>Se connecter</h2><br>
                <form action='connectionProprio' method='POST'>
                    <label for="numRegistre">Numéro de registre:</label>
                    <input type="text" name="numRegistre" id="numRegistre" value="${numRegistre}" required size="15"/><br/>

                    <label for="motdepasse">Mot de passe:</label>
                    <input type="password" name="motdepasse" id="motdepasse" required/><br/>

                    <em>${proprioIncorrect}</em>
                    <br/><br/><input type="submit" value="Valider" class="btn btn-primary">
                </form>

            </div>
            <hr>
            <div>
                <h3>Pas de compte?</h3><br>
                <a href="createAccount" class="btn btn-secondary">Créer un compte</a>
            </div>
        </div>
    </body>
</html>