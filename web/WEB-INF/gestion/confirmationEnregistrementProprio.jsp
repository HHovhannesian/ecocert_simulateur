<%-- 
    Document   : confirmationEnregistrementProprio
    Created on : 26 mars 2021, 08:42:26
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container text-center">
            <h2>Enregistrement d'un propriétaire</h2><br/>

            <h3>Votre compte a bien été créé!</h3><br>
            <b>Dossier ${proprioCree.numRegistre}:</b> ${proprioCree.nomProprietaire}<br/>
            <b>Date de début d'activité:</b> ${dateDebutActivite}<br/><br>
            
            <a href="connectionProprio" class="btn btn-primary">Connexion</a><br><br><br>
            <a href="accueil" class="btn btn-secondary">Retour à l'accueil</a>
            
        </div>
    </body>
</html>