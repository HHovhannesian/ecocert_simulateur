<%-- 
    Document   : detailSimulation
    Created on : 5 mai 2021, 14:07:05
    Author     : ines.feugier
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp"%>
        <div class="container text-center">
            <h2>Affichage des informations</h2><br/>
            <div>
                Identifiant de la simulation: ${laSim["id"]}<br><br>
                Date de la simulation : ${laSim["dateSim"]}<br/>
                Simulation sur la région : ${laSim["region"]}<br/>
                Date de début de conversion : ${laSim["dateDebConv"]}<br/><br/>
                Chiffre d'affaires par parcelle :
            </div>
            <!-- Affichage du détail avec JSTL-->
            <table>
                <tr> 
                    <th> </th>
                    <th>Année</th>
                    <th>Nom culture</th>
                    <th>Etat</th>
                    <th>Date recolte</th>
                    <th>CA</th>
                </tr>
                <c:set var="numParcelle2" value="1"></c:set>
                <c:forEach items="${parcelles}" var="uneParcelle">
                    <tr>
                        <!--Le numéro de la parcelle-->
                        <td>Parcelle ${numParcelle2}</td>
                        <!--L'année de récolte-->
                        <td>
                            <c:forEach items="${lignesCA[numParcelle2-1]}" var="uneLigneCA">
                                ${uneLigneCA.anRec}<br>
                            </c:forEach>
                        </td>
                        <!--Le nom de la culture-->
                        <td>
                            <c:if test="${uneParcelle.typeParcelle == 1}">
                                ${uneParcelle.laCultureP.libelle}
                            </c:if>
                            <c:if test="${uneParcelle.typeParcelle == 2}">
                                <c:forEach items="${uneParcelle.lesCultures}" var="laCultA">
                                    ${laCultA.libelle}<br>
                                </c:forEach>
                            </c:if>
                        </td>
                        <!--L'état de la culture-->
                        <td>
                            <c:forEach items="${lignesCA[numParcelle2-1]}" var="uneLigneCA">
                                ${uneLigneCA.anRec}<br>
                            </c:forEach>
                        </td>
                        <!--La date de récolte-->
                        <td>
                            <c:forEach items="${lignesCA[numParcelle2-1]}" var="uneLigneCA">
                                ${format2.format(uneLigneCA.laDateRec.getTime())}<br>
                            </c:forEach>
                        </td>
                        <!--Le chiffre d'affaire-->
                        <td>
                            <c:forEach items="${lignesCA[numParcelle2-1]}" var="uneLigneCA">
                                ${uneLigneCA.CA}<br>
                            </c:forEach>
                        </td>
                    </tr>
                    <c:set var="numParcelle2" value="${numParcelle2 + 1}"></c:set>
                </c:forEach>
            </table>
    </body>
</html>