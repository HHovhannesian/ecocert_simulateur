<%-- 
    Document   : enregistrementProprio
    Created on : 25 mars 2021, 17:12:10
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container text-center">
            <h2>Enregistrement d'un propriétaire</h2><br>
            <form action='createAccount' method='POST'>
                <label for="numRegistre">Numéro de registre:</label>
                <input type="text" name="numRegistre" id="numRegistre" value="${numRegistre}" required size="20"/><br/>

                <label for="nom">Nom:</label>
                <input type="text" name="nom" id="nom" value="${nom}" required size="32"/><br/>

                <label for="dateDebut">Date de début d'activité:</label>
                <input type="date" name="dateDebut" id="dateDebut" value="${dateDebut}"/><br/><br/>

                <label for="motdepasse">Mot de passe:</label>
                <input type="password" name="motdepasse" id="motdepasse" required size="27"/><br/>

                <label for="motdepasseConf">Confirmez le mot de passe:</label>
                <input type="password" name="motdepasseConf" id="motdepasseConf" required size="16" /><br/><br/>
                
                <em>${proprioIncorrect}</em>

                <br/><br/><input type="submit" value="Valider" class="btn btn-success">
            </form>
        </div>
    </body>
</html>