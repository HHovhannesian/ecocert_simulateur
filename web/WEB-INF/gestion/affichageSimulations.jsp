<%-- 
    Document   : affichageSimulations
    Created on : 1 avr. 2021, 16:48:27
    Author     : ines.feugier
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp"%>
        <div class="container text-center">
            <h2>Liste des simulations enregistrées</h2><br/>
            <!-- Tableau avec récupération de données en JSTL -->
            <table>
                <tr>
                    <th>Region</th>
                    <th>Date simulation</th>
                    <th>Date début conversion</th>
                    <th>Nombre de parcelles</th>
                    <th>Détail</th>
                    <th>Suppression</th>
                </tr>
                <!-- Pour chaque simulation de la liste -->
                <c:forEach items="${lesSimulations}" var="laSim">
                    <!-- Afficher les données dans les cases du tableau-->
                    <tr>
                        <td>${laSim["region"]}</td>
                        <td>${laSim["dateSim"]}</td>
                        <td>${laSim["dateDebConv"]}</td>
                        <td>${laSim["nbParcelles"]}</td>
                        <td>
                            <form action="detailSimulation" method="POST">
                                <input type="hidden" name="idSim" value="${laSim["id"]}">
                                <button type="submit" href="#"><i class="fas fa-eye"></i></button>
                            </form>
                        </td>
                        <td>
                            <form action="suppressionSimulation" method="POST">
                                <input type="hidden" name="idSim" value="${laSim["id"]}">
                                <button type="submit" href="#"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
