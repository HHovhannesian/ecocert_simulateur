<%-- 
    Document   : suppressionSucces
    Created on : 6 mai 2021, 08:48:49
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp"%>
        <div class="container text-center">
            <h2>Succès de la suppression</h2><br>
            <a href="accueil" class="btn btn-secondary">Retour à l'accueil</a>
        </div>
    </body>
</html>