<%-- 
    Document   : pasProprietaireSimulation
    Created on : 6 mai 2021, 08:46:25
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp"%>
        <div class="container">
            <div class="text-center">
                <h2>La simulation n'appartient pas au propriétaire connecté</h2>
            </div>
        </div>
    </body>
</html>
