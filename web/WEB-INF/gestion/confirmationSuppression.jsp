<%-- 
    Document   : confirmationSupression
    Created on : 5 mai 2021, 14:08:14
    Author     : ines.feugier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp"%>
        <div class="container text-center">
            <form action="confirmationSuppression" method="POST">
                <h2>Supprimer la simulation n°${laSim.id}?</h2><br/>
                <input type="hidden" name="idSim" value="${laSim.id}">
                <button type="submit" href="#" class="btn btn-danger">
                    <i class="fas fa-trash"></i> Supprimer
                </button>
            </form>
        </div>
    </body>
</html>