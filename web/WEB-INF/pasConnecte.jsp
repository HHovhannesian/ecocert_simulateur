<%-- 
    Document   : pasConnecte
    Created on : 1 avr. 2021, 16:55:21
    Author     : ines.feugier
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container"><br><br>
            <div class="text-center">
                <h2>Vous n'êtes pas connecté</h2>
                <a href="connectionProprio">
                    Veuillez vous connecter pour continuer
                </a>
            </div>
        </div>
    </body>
</html>
