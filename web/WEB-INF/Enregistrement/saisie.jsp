<%-- 
    Document   : saisie
    Created on : 25 mars 2021, 16:32:51
    Author     : hugo.hovhannessian
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="beans.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/entete.jsp"%>
        <title>Ecocert | Simulateur </title>
    </head>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <div class="container">
            <h1>Simulation de culture perenne</h1>
            <form action='simulation' method='POST'>
                <label>Region : </label>
                <select name="region">
                    <option value="" selected disabled>Sélectionner</option>
                    <%
                        ArrayList<Region> lesRegions = (ArrayList<Region>) request.getAttribute("lesRegions");
                        for (Region uneRegion : lesRegions) {
                            out.println("<option value=" + uneRegion.getNumRegion() + ">");
                            out.println(uneRegion.getNomRegion());
                            out.println("</option>");
                        }
                    %>
                </select>

                <br>
                <label>Date de début de conversion : </label>
                <input type="date" name="dateDebConv">

                <br>
                <label>Parcelles pérennes : </label>
                <div class="container">
                    <div class="container">
                        <label>Surface</label>
                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 
                        <label>Culture</label>
                        <br>
                        <input type="number" name="sufaceP1">
                        <select name="cultureP1">
                            <option value="" selected>Sélectionner</option>
                            <%
                                ArrayList<Culture> lesCultures = (ArrayList<Culture>) request.getAttribute("lesCultures");
                                for (Culture uneCulture : lesCultures) {
                                    out.println("<option value=" + uneCulture.getNumCulture()+ ">");
                                    out.println(uneCulture.getLibelle());
                                    out.println("</option>");
                                }
                            %>
                        </select>
                    </div>

                    <div class="container">
                        <label>Surface</label>
                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 
                        <label>Culture</label>
                        <br>
                        <input type="number" name="sufaceP2">
                        <select name="cultureP2">
                            <option value="" selected>Sélectionner</option>
                            <%
                                for (Culture uneCulture : lesCultures) {
                                    out.println("<option value=" + uneCulture.getNumCulture() + ">");
                                    out.println(uneCulture.getLibelle());
                                    out.println("</option>");
                                }
                            %>
                        </select>
                    </div>
                </div>
                <input type="submit">
                
            </form>
        </div>
    </body>
</html>
