<%-- 
    Document   : recapitulatifSaisie
    Created on : 26 mars 2021, 09:54:46
    Author     : hugo.hovhannessian
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/entete.jsp"%>
    <body>
        <%@include file="/WEB-INF/nav.jsp" %>
        <h3> <b>Affichage des informations</b></h3>
        <div>
            <p  align="left">Date de la simulation : ${laSimul.dateSimulation}<br/>
            Simulation sur la region : ${laSimul.laRegion}<br/>
            Date de début de convention : ${laSimul}</p><br/>
        </div>
       
        <p align="left">&emsp;&emsp;&emsp;Chiffre d'affaires par parcelle : </p>
        <table>
            <thead>
                <tr align="center"> 
                    <td> </td>
                    <td>Année</td>
                    <td>Nom culture</td>
                    <td>Etat</td>
                    <td>Date recolte</td>
                    <td>CA</td>
                </tr>
               
            </thead>
            <tbody style="border: 1px solid black">
                
            </tbody>
        </table>
    </body>
</html>
<style>
    table,
td {
    border: 1px solid #333;
}

thead,
tfoot {
    background-color: #333;
    color: #fff;
}
</style>
