/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import passerelle.CultureDao;
import passerelle.RegionDao;
import passerelle.*;
import beans.Culture;
import beans.Region;
import java.util.Calendar;
import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bc
 */
public class CultureTest {

    private static Culture c;
    private static Region r;

    public CultureTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        r = RegionDao.getRegion(2);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDateRec method, of class Culture.
     */
    @Test
    public void testGetDateRec() {
        //Cas pour une culture perenne (Luzerne) et une date de debut de conversion au 1 septembre 2020
        c = CultureDao.getCulture(5);
        Calendar dateDebConversion = Calendar.getInstance();
        dateDebConversion.set(2020, 9, 1);
        //La première date de récolte est au 1 juin 2021 et s'incrémente de 1 à chaque récolte
        Calendar dateRecolte = Calendar.getInstance();
        dateRecolte.set(2021, 5, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 1, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 1, dateDebConversion).get(Calendar.MONTH));
        dateRecolte.set(2022, 5, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 2, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 2, dateDebConversion).get(Calendar.MONTH));
        dateRecolte.set(2023, 5, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 3, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 3, dateDebConversion).get(Calendar.MONTH));
        //Pour un numéro de récolte invalide (0)
        assertEquals(Calendar.getInstance().get(Calendar.YEAR), c.getDateRec(r, 0, dateDebConversion).get(Calendar.YEAR));
        assertEquals(Calendar.getInstance().get(Calendar.MONTH), c.getDateRec(r, 0, dateDebConversion).get(Calendar.MONTH));

        //Cas pour une culture annuelle (Mais) et une date de debut de conversion au 1 septembre 2020
        c = CultureDao.getCulture(7);
        //La première date de récolte est au 1 juin 2021 et s'incrémente de 1 à chaque récolte
        dateRecolte.set(2020, 10, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 1, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 1, dateDebConversion).get(Calendar.MONTH));
        dateRecolte.set(2021, 10, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 2, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 2, dateDebConversion).get(Calendar.MONTH));
        dateRecolte.set(2022, 10, 1);
        assertEquals(dateRecolte.get(Calendar.YEAR), c.getDateRec(r, 3, dateDebConversion).get(Calendar.YEAR));
        assertEquals(dateRecolte.get(Calendar.MONTH), c.getDateRec(r, 3, dateDebConversion).get(Calendar.MONTH));
        //Pour un numéro de récolte invalide (0)
        assertEquals(Calendar.getInstance().get(Calendar.YEAR), c.getDateRec(r, 0, dateDebConversion).get(Calendar.YEAR));
        assertEquals(Calendar.getInstance().get(Calendar.MONTH), c.getDateRec(r, 0, dateDebConversion).get(Calendar.MONTH));
    }

    /**
     * Test of getCARec method, of class Culture.
     */
    @Test
    public void testGetCARec() {
        //cas pour une culture perenne (Luzerne) et une surface de parcelle de 20 hectares
        c = CultureDao.getCulture(5);
        assertEquals(36400, c.getCARec(1, 20), 0); //en conventionnel
        assertEquals(42000, c.getCARec(2, 20), 0); //en conversion
        assertEquals(47600, c.getCARec(3, 20), 0); //en bilologique
        //cas pour une culture perenne (Sainfoin) et une surface de parcelle de 10 hectares
        c = CultureDao.getCulture((6));
        assertEquals(18000, c.getCARec(1, 10), 0); //en conventionnel
        assertEquals(19000, c.getCARec(2, 10), 0); //en conversion
        assertEquals(20000, c.getCARec(3, 10), 0); //en bilologique
    }

}
